from django.apps import AppConfig


class SecmonitorAppConfig(AppConfig):
    name = 'secmonitor_app'
