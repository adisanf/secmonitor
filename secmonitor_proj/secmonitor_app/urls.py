"""secmonitor_proj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URL conf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from secmonitor_app.views import first_page, second_page, third_page, forth_page, fifth_page, system_page
from . views import AboutView


urlpatterns = [
    path('first_page/', first_page, name='first_page'),
    path('second_page/', second_page, name='second_page'),
    path('third_page/', third_page, name='third_page'),
    path('system_page/', system_page, name='system_page'),
    path('forth_page/', forth_page, name='forth_page'),
    path('fifth_page/', fifth_page, name='fifth_page'),
    path('about/', AboutView.as_view()),
]
