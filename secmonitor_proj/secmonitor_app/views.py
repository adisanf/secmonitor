import paramiko
from django.shortcuts import render
from django.views.generic import TemplateView
from .forms import Client


def system_page(request):
    context = {}
    form = Client()
    context['form'] = form
    return render(request, 'system_page.html', context)


def first_page(request):
    if request.method == 'POST':
        temp = request.POST
        # ip_win1 = '********'
        # user = '*******'
        # pass_1 = '******'
        ip_win1 = temp.get('ip', '')
        user = temp.get('username', '')
        pass_1 = temp.get('password', '')
        hdd = printare_hdd(ip_win1, user, pass_1)
        cpu = printare_cpu(ip_win1, user, pass_1)
        ram = printare_rammemory(ip_win1, user, pass_1)
        net = printare_nets(ip_win1, user, pass_1)
        gpo = printare_gpo(ip_win1, user, pass_1)

        return render(request, 'system_win_details.html', {'hdd_line': hdd, 'cpu_line': cpu, 'ram_line': ram,
                      'gpo_line': gpo, 'net_line': net})


def printare_hdd(ip_win1, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win1, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('wmic logicaldisk get size,freespace,caption')
    b = stdout.readlines()
    client.close()
    return b


def printare_cpu(ip_win1, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win1, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('wmic cpu get maxclockspeed')
    c = stdout.readlines()
    c1 = []
    for element in c:
        c1.append(element.strip())
    client.close()
    return c1


def printare_rammemory(ip_win1, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win1, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('wmic MEMORYCHIP get BankLabel, Speed, Capacity')
    d = stdout.readlines()
    d1 = []
    for element in d:
        d1.append(element.strip())
    client.close()
    return d1


def printare_gpo(ip_win1, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win1, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('net accounts')
    g = stdout.readlines()
    g1 = []
    for element in g:
        g1.append(element.strip())
    client.close()
    return g1


def printare_nets(ip_win1, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win1, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('NETSH INT IP SHOW interface')
    e = stdout.readlines()
    e1 = []
    for element in e:
        e1.append(element.strip())
    client.close()

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win1, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('route print -4')
    e = stdout.readlines()
    e2 = []
    for element in e:
        e2.append(element.strip())
    client.close()
    return e1 + e2


def third_page(request):

    if request.method == 'POST':
        temp = request.POST
        # ip_win_sv = '*********'
        # user = '**********'
        # pass_1 = '*********'
        ip_win_sv = temp.get('ip', '')
        user = temp.get('username', '')
        pass_1 = temp.get('password', '')

        hdd2 = printare_hdd2(ip_win_sv, user, pass_1)
        cpu2 = printare_cpu2(ip_win_sv, user, pass_1)
        ram2 = printare_rammemory2(ip_win_sv, user, pass_1)
        gpo2 = printare_gpo2(ip_win_sv, user, pass_1)
        net2 = printare_nets2(ip_win_sv, user, pass_1)

        return render(request, 'system_winserver_details.html', {'hdd_line': hdd2, 'cpu_line': cpu2, 'ram_line': ram2,
                      'gpo_line': gpo2, 'net_line': net2})


def printare_hdd2(ip_win_sv, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win_sv, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('wmic logicaldisk get size,freespace,caption')
    bb = stdout.readlines()
    client.close()
    return bb


def printare_cpu2(ip_win_sv, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win_sv, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('wmic cpu get maxclockspeed')
    c = stdout.readlines()
    c11 = []
    for element in c:
        c11.append(element.strip())
    client.close()
    return c11


def printare_rammemory2(ip_win_sv, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win_sv, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('wmic MEMORYCHIP get Speed, Capacity')
    d = stdout.readlines()
    d11 = []
    for element in d:
        d11.append(element.strip())
    client.close()
    return d11


def printare_gpo2(ip_win_sv, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win_sv, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('net accounts')
    g = stdout.readlines()
    g11 = []
    for element in g:
        g11.append(element.strip())
    client.close()
    return g11


def printare_nets2(ip_win_sv, user, pass_1):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win_sv, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('NETSH INT IP SHOW interface')
    e = stdout.readlines()
    e11 = []
    for element in e:
        e11.append(element.strip())
    client.close()

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_win_sv, username=user, password=pass_1)
    stdin, stdout, stderr = client.exec_command('route print -4')
    e = stdout.readlines()
    e22 = []
    for element in e:
        e22.append(element.strip())
    client.close()
    return e11 + e22


def second_page(request):

    if request.method == 'POST':
        temp = request.POST
        # user = '*********'
        # pass_l = '********'
        # ip_lin = '*********'
        ip_lin = temp.get('ip', '')
        user = temp.get('username', '')
        pass_l = temp.get('password', '')
        ver = printare_linux_os(ip_lin, user, pass_l)
        hddlin = printare_linux_hdd(ip_lin, user, pass_l)
        ramlin = printare_linux_ram_net(ip_lin, user, pass_l)
        userlin = printare_linux_users(ip_lin, user, pass_l)
        folders = printare_linux_folders(ip_lin, user, pass_l)

        return render(request, 'system_linux_details.html', {'ver_line': ver, 'hddlin_line': hddlin, 'ramlin_line':
                      ramlin, 'userlin_line': userlin, 'folderslin_line': folders})


def printare_linux_os(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('uname -a')
    output = stdout.readlines()
    a1 = output
    client.close()
    return a1


def printare_linux_hdd(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('df -h')
    output = stdout.readlines()
    b1 = output
    client.close()
    return b1


def printare_linux_users(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command(
        'cat /etc/passwd | grep -v false | grep -v nologin && cat /etc/shadow | grep -v false | grep -v nologin')
    output = stdout.readlines()
    b2 = output
    client.close()
    return b2


def printare_linux_ram_net(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('free -h && ip a | grep inet | grep -v inet6 && ip r')
    output = stdout.readlines()
    c1 = output
    client.close()
    return c1


def printare_linux_folders(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('pwd && cd /var/log && du -sh')
    output = stdout.readlines()
    c2 = output
    client.close()
    return c2


def forth_page(request):

    if request.method == 'POST':
        temp = request.POST
        ip_lin = temp.get('ip', '')
        user = temp.get('username', '')
        pass_l = temp.get('password', '')
        ver = printare_ios(ip_lin, user, pass_l)
        hddlin = printare_ios_hdd(ip_lin, user, pass_l)
        ramlin = printare_ios_ram_net(ip_lin, user, pass_l)
        userlin = printare_ios_users(ip_lin, user, pass_l)
        folders = printare_ios_folders(ip_lin, user, pass_l)

        return render(request, 'system_ios_details.html', {'ver_line': ver, 'hddlin_line': hddlin, 'ramlin_line':
                      ramlin, 'userlin_line': userlin, 'folderslin_line': folders})


def printare_ios(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('show version | include Cisco')
    output = stdout.readlines()
    a1 = output
    client.close()
    return a1


def printare_ios_hdd(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('show file systems | include disk')
    output = stdout.readlines()
    b1 = output
    client.close()

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('show file systems | include nvram')
    output = stdout.readlines()
    b2 = output
    client.close()
    return b1+b2


def printare_ios_users(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('show users')
    output = stdout.readlines()
    b3 = output
    client.close()

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('show access-list')
    output = stdout.readlines()
    b4 = output
    client.close()

    return b3 + b4


def printare_ios_ram_net(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('show ip interface brief | include up')
    output = stdout.readlines()
    c1 = output
    client.close()
    return c1


def printare_ios_folders(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('show flash:')
    output = stdout.readlines()
    c2 = output
    client.close()
    return c2


def fifth_page(request):

    if request.method == 'POST':
        temp = request.POST
        ip_lin = temp.get('ip', '')
        user = temp.get('username', '')
        pass_l = temp.get('password', '')
        ver = printare_cc(ip_lin, user, pass_l)
        hddlin = printare_cc_hdd(ip_lin, user, pass_l)
        ramlin = printare_cc_net(ip_lin, user, pass_l)
        userlin = printare_cc_users(ip_lin, user, pass_l)
        config = printare_cc_configuration(ip_lin, user, pass_l)

        return render(request, 'system_cc_details.html', {'ver_line': ver, 'hddlin_line': hddlin, 'ramlin_line':
                      ramlin, 'userlin_line': userlin, 'folderslin_line': config})


def printare_cc(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('uname -a & uname -r')
    output = stdout.readlines()
    a1cc = output
    client.close()
    return a1cc


def printare_cc_hdd(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('df -h & free -h')
    output = stdout.readlines()
    b1cc = output
    client.close()

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('cphaprob stat')
    output = stdout.readlines()
    b2cc = output
    client.close()
    return b1cc+b2cc


def printare_cc_users(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('cat /etc/passwd | grep -v /nologin')
    output = stdout.readlines()
    b3cc = output
    client.close()

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('whoami')
    output = stdout.readlines()
    b4cc = output
    client.close()

    return b3cc + b4cc


def printare_cc_net(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('ip a | grep inet & ip r')
    output = stdout.readlines()
    c1cc = output
    client.close()
    return c1cc


def printare_cc_configuration(ip_lin, user, pass_l):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip_lin, username=user, password=pass_l)
    stdin, stdout, stderr = client.exec_command('clish -c "show configuration"')
    output = stdout.readlines()
    c2cc = output
    client.close()
    return c2cc


class AboutView(TemplateView):
    template_name = "about.html"
