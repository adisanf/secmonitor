from django import forms


class Client(forms.Form):
    ip = forms.CharField(max_length=15)
    username = forms.CharField(max_length=24)
    password = forms.CharField(max_length=24)
